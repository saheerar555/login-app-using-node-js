var express = require("express");
var router = express.Router();

/* GET users listing. */
router.get("/profile", function(req, res, next) {
  if (!req.user) {
    res.redirect("/auth/login");
  } else {
    res.render("profile");
  }
});

module.exports = router;
